<!-- perfil.php -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Dashboard">
	<meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

	<title>Bienvenido al Perfil</title>

	<!-- Bootstrap core CSS -->
	<link href="assets/css/bootstrap.css" rel="stylesheet">
	<!--external css-->
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
	<link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">

	<!-- Custom styles for this template -->
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/style-responsive.css" rel="stylesheet">

	<script src="assets/js/chart-master/Chart.js"></script>


</head>

<body>

	<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
      	<div class="sidebar-toggle-box">
      		<div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      	</div>
      	<!--logo start-->
      	<a href="index.html" class="logo"><b>perfil</b></a>
      	<!--logo end-->
      </div>
    </header>
    <!--header end-->

      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
      	<div id="sidebar"  class="nav-collapse ">
      		<!-- sidebar menu start-->
      		<ul class="sidebar-menu" id="nav-accordion">

      			<p class="centered"><a href="profile.html"><img src="assets/img/user_img.png" height="60" class="img-circle" width="60"></a></p>
      			<h5 class="centered">Usuario</h5>
      			<!-- OPCIONES DEL MENU DEL ADMINISTRADOR --
      			<!-- MENU DE USUARIOS -->
      			<li class="sub-menu ">
      				<a href="#" class="active">
      					<i class="fa fa-bar-chart-o "></i>
      					<span>PROPUESTA</span>
      				</a>
      				<ul class="sub ">
      					<li class="active"><a href="form_reg_usu.html">Enviar Propuesta</a></li>
      					<!-- <li><a href="listar_edit_usu.html">Listar y Modificar Usuarios</a></li> -->
      				</ul>
      			</li>
      			<li class="sub-menu">
      				<a href="#"><i class="fa fa-cogs"></i>
      					<span>CUENTA</span>
      				</a>
      				<ul class="sub">
      					<!-- <li><a href="#">Configuración</a></li> -->
      					<li><a class="logout"href="index.php?action=deslogear">Salir</a></li>
      				</ul>
      			</li>


      		</ul>
      		<!-- sidebar menu end-->
      	</div>
      </aside>
      <!--sidebar end-->

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->

      <section id="main-content">
      	<section class="wrapper">
          <div class="container">
            <div class="row mt">
              <div class="col-lg-12">
                <div class="form-panel">
                  <!-- <a href="index.html">REGRESAR</a> -->
                  <h3 class="mb text-center"><strong>FORMULARIO DE PROPUESTA</strong></h3>
                  <form class="form-horizontal style-form" method="post" action="index.php?action=generar_pdf">
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2  text-center"><strong>texto_1</strong></label>
                      <div class="col-sm-10">
                        <input required type="text" name="texto1" class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>texto_2</strong></label>
                      <div class="col-sm-10">
                        <input required type="text" name="texto2" class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>texto_3</strong></label>
                      <div class="col-sm-10">
                        <input required type="text" name="texto3" class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>Asunto</strong></label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" rows="3" name="asunto">
                        <!-- <input required type="password" name="pass" class="form-control" placeholder=""> -->
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>texto_4</strong></label>
                      <div class="col-sm-10">
                        <textarea class="form-control" rows="3" name="texto4"></textarea>
                        <!-- <input required type="password" name="pass" class="form-control" placeholder=""> -->
                      </div>
                    </div>                    
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>texto_5</strong></label>
                      <div class="col-sm-10">
                        <input required type="text"  name="texto5" class="form-control" placeholder="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>texto_6</strong></label>
                      <div class="col-sm-10">
                        <input required type="text" class="form-control" name="texto6">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>texto_7</strong></label>
                      <div class="col-sm-10">
                        <input required type="numb" class="form-control" name="texto7">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>texto_8</strong></label>
                      <div class="col-sm-10">
                        <input required type="text"  class="form-control" name="texto8">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>texto_9</strong></label>
                      <div class="col-sm-10">
                        <input required type="text"  class="form-control" name="texto9">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>texto_10</strong></label>
                      <div class="col-sm-10">
                        <!-- <input required type="text"  class="form-control" name="texto_10"> -->
                        <textarea class="form-control" rows="3" name="texto10"></textarea>

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2  text-center"><strong>Tiempo de Entrega</strong></label>
                      <div class="col-sm-10">
                        <select name="tiempo"  class="form-control">
                          <option value="Inmediato" selected="selected">Inmediato</option>
                          <option selected value="Normal" selected="selected">Normal</option>
                        </select>
                      </div>
                    </div>                    
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 text-center"><strong>texto_11</strong></label>
                      <div class="col-sm-10">
                        <input required type="text"  class="form-control" name="texto11">
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-6 text-center">
                        <button class="btn btn-lg btn-success" type="submit">Enviar</button>
                      </div>
                      <div class="col-sm-6 text-center">
                        <button class="btn btn-lg btn-warning" type="reset">Reset</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div><!-- col-lg-12-->
            </div>

          </section>
        </section>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/jquery-1.8.3.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="assets/js/jquery.sparkline.js"></script>


        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>

        <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
        <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

        <!--script for this page-->
        <script src="assets/js/sparkline-chart.js"></script>
        <script src="assets/js/zabuto_calendar.js"></script>

        <!-- VENTANA DE NOTIFICACION O DE SALUDO -->
        <script type="text/javascript">
      // $(document).ready(function () {
      // 	var unique_id = $.gritter.add({
      //       // (string | mandatory) the heading of the notification
      //       title: 'Bienvenido al Sistema!',
      //       // (string | mandatory) the text inside the notification
      //       text: 'Has iniciado correctamente la sesión.',
      //       // (string | optional) the image to display on the left
      //       image: 'assets/img/not_bad.jpg',
      //       // (bool | optional) if you want it to fade out on its own or just sit there
      //       sticky: true,
      //       // (int | optional) the time you want it to be alive for before fading out
      //       time: '',
      //       // (string | optional) the class name you want to apply to that specific message
      //       class_name: 'my-sticky-class'
      //   });

      // 	return false;
      // });
</script>

<script type="application/javascript">
$(document).ready(function () {
 $("#date-popover").popover({html: true, trigger: "manual"});
 $("#date-popover").hide();
 $("#date-popover").click(function (e) {
  $(this).hide();
});

 $("#my-calendar").zabuto_calendar({
  action: function () {
   return myDateFunction(this.id, false);
 },
 action_nav: function () {
   return myNavFunction(this.id);
 },
 ajax: {
   url: "show_data.php?action=1",
   modal: true
 },
 legend: [
 {type: "text", label: "Special event", badge: "00"},
 {type: "block", label: "Regular event", }
 ]
});
});

function myNavFunction(id) {
 $("#date-popover").hide();
 var nav = $("#" + id).data("navigation");
 var to = $("#" + id).data("to");
 console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
}
</script>


</body>
</html>
