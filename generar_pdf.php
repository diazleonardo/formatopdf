<!-- generar_pdf.php -->

<?php
session_start();
// require_once("dompdf/dompdf_config.inc.php");
require_once 'dompdf/autoload.inc.php';

use Dompdf\Dompdf;
use Dompdf\Options;
// echo "<script>alert('paso');</script>";
// print_r($_POST);


$html= "<!-- Bootstrap core CSS -->
<style>
html {
  margin: 0;
}
body {
  font-family: 'Times New Roman', serif;
  margin: 45mm 8mm 2mm 8mm;
}
hr {
  page-break-after: always;
  border: 0;
  margin: 0;
  padding: 0;
}
</style>
	<link href='assets/css/bootstrap.css' rel='stylesheet'>
	<!--external css-->
  <link href='assets/font-awesome/css/font-awesome.css' rel='stylesheet' />


	<!-- Custom styles for this template -->
	
	<link href='assets/css/style-responsive.css' rel='stylesheet'>

	<!-- // <script src='assets/js/chart-master/Chart.js'></script> -->

  <!-- <section id='main-content'> -->
  <!-- <section class='wrapper'> -->
  <div class='container'>
    <div class='row mt'>
      <div class='col-lg-12'>
        <div class='form-panel' style='padding:5em;'  style='height:595.28px; width:841px;'>
          <div class='row'>
            <div class='col-lg-12'>
            <h1>LOGO</h1>
            </div>
          </div>
          <div class='row'>
            <div class='col-lg-6'>
              <h4>Señores</h4> <br>
              <strong>".$_SESSION['texto1']."</strong><br>
              Sus manos<br>
            </div>
            <div class='col-lg-6' style='left:25em; top:15em; position:absolute;'>
             <strong>Ejecutivo: ".$_SESSION['nombre'].' '.$_SESSION['apellido']."</strong>  <br>
             <strong>Referencia: ".$_SESSION['referencia']." </strong> <br>
             <strong>Teléfono:".$_SESSION['telefono']." </strong> <br>
             <strong>Celular: ".$_SESSION['celular']."</strong> <br>
             <strong>E-mail: ".$_SESSION['email']."</strong> <br>
             <strong>Fecha: </strong> <br><br>
           </div>
         </div>
         <div class='row'>
          <div class='col-lg-12'>
            <div>
             <strong >Atención:</strong>
             <strong style='left:5em;'><ins>".$_SESSION['texto2']."</ins></strong><br>
             <div style='margin-left:5em;'>".$_SESSION['texto3']."</div>
           </div>
           <br><br>
         </div>
       </div>
       <div class='row'>
        <div class='col-lg-12'>
          <strong>Asunto:</strong>
          <strong>".$_SESSION['asunto']."</strong>
          <br><br>
        </div>
      </div>
      <div class='row'>
        <div class='col-lg-12'>
          <p>
            ".$_SESSION['texto4']."
          </p>
        </div>
      </div>
      <div class='row'>
        <div class='col-lg-12'>
          <br><br><br>
          <table class='table table-bordered'>
            <thead>
            <tr>
              <td>POS</td>
              <td>DESCRIPCION</td>
              <td>CANT</td>
              <td>PRECIO UNITARIO U$</td>
              <td>PRECIO TOTAL U$</td>
            </tr>              
            </thead>
            <tbody>
              <tr>
                <td>".$_SESSION['texto5']."</td>
                <td>".$_SESSION['texto6']."</td>
                <td>".$_SESSION['texto7']."</td>
                <td>".$_SESSION['texto8']."</td>
                <td>".$_SESSION['texto9']."</td>
              </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>                                          
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><strong>SUBTOTAL</strong></td>
                <td>calculo de subtotal</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><strong>IVA (15%)</strong></td>
                <td>calculo más el iva</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><strong>TOTAL</strong></td>
                <td>calculo total</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class='row' >
        <div class='col-lg-12'>
          <div class='text-center' style='font-size:10px; position:absolute; top:15em;left:18em;'>
            Datos de la empresa. Favor dejar dos líneas máximo, con Font 10px.
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- </section> -->
<!-- SEGUNDA HOJA -->
<!-- <section class='wrapper ' > -->
<div class='container' >
  <div class='row mt'>
    <div class='col-lg-12'>
      <div class='form-panel' style='padding:5em;'>
        <div class='row'>
          <div class='col-lg-12'>
            <div style='margin-top:5em; margin-bottom:5em;'>
            <h1>LOGO</h1>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class='col-lg-12'>
            <div class='detalles-pago'>
              <strong>Forma de pago:</strong> 
              Contado, Tarjeta de crédito, Cheque <br><br>
              <strong>Tiempo de entrega:</strong>".$_SESSION['tiempo']."<br><br>
              <strong>Validez de la oferta:</strong> Treinta (30) días a partir de esta fecha <br> <br>
            </div> 
            <div class='row'>
              <div class='col-lg-12'>
               <div class='texto_10'>
                 <p>
                 ".$_SESSION['texto10']."
                </p>
              </div>
              <br>
              <br>
            </div>
          </div>
          <div class='row'>
            <div class='col-lg-12'>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
            </div>
          </div>
          <div class='row'>
            <div class='col-lg-12'>
             <p>
              En espera de que nuestra oferta sea de su agrado y conveniencia, y siempre a la orden para cualquier aclaración de la presente, nos suscribimos.

              Muy atentamente,
            </p>
          </div>
        </div>
        <div class='row'>
          <div class='col-lg-12'>
            <div class='datos-empresa text-center' style='font-size:10px; position:absolute; top:30em;left:20em;'>
              Datos de la empresa. Favor dejar dos líneas máximo, con Font 10px.
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>";

$dompdf = new Dompdf();

// $dompdf->set_option('chroot','/var/www/html/dibujar_pdf/formato');
// echo $dompdf->get_option('chroot');
$dompdf->set_paper("letter", "portrait");

// $paper_size = array(0,0,360,360);
// $dompdf->set_paper($paper_size);
//Cargamos nuestro código HTML
$dompdf->load_html(html_entity_decode($html));

//Hacemos la conversion de HTML a PDF
$dompdf->render();

//El nombre con el que deseamos guardar el archivo generado
$dompdf->stream(date('d_m_Y')."_".$_SESSION['nombre']."_".$_SESSION['apellido']);

header('location:index.php?action=perfil');


?>