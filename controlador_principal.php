<!-- controlador_principal.php -->
<?php 
session_start();

class controlador_principal {
	public static function iniciar(){
		$action=isset($_GET['action']) ? $_GET['action']:"principal";
		switch ($action){
			case 'principal':controlador_principal::principal();break;

			case 'deslogear':controlador_principal::usuario('deslogear');break;

			case 'logear':controlador_principal::usuario('logear');break;

			case 'enviarPropuesta':controlador_principal::usuario('propuesta');break;

			case 'registrar':controlador_principal::usuario('registrar');break;

			case 'validar_datos':controlador_principal::usuario('validar_datos');break;

			case 'perfil':controlador_principal::usuario('perfil');break;

			case 'generar_pdf':controlador_principal::usuario('generar_pdf');break;
			
			default:controlador_principal::principal();break;
		}
	}
	public static function usuario($action_usuario){
		require_once "controlador_usuario.php";
		if($action_usuario=='deslogear'){
			controlador_usuario::cerrar_sesion();
		}
		if($action_usuario=='logear'){
			controlador_usuario::iniciar_sesion();
		}
		if ($action_usuario=='propuesta'){

		}
		if($action_usuario=='generar_pdf'){
			controlador_usuario::generar_pdf();
		}
		if($action_usuario=='formular_propuesta'){
			controlador_usuario::formulario_propuesta();
		}
		if($action_usuario=='registrar'){
			if(!isset($_SESSION['nombre'])){
				controlador_usuario::registrar();
			}
			else{
				header('location:index.php?action=perfil');
			}
		}
		if($action_usuario=='validar_datos'){
			controlador_usuario::validar_datos();
		}
		if ($action_usuario=='perfil') {
			if(isset($_SESSION['nombre'])){
				controlador_usuario::mostrar_perfil();
			}
			else{
				header('location: index.php?action=principal');
			}
		}
	}
	public static function principal(){
		if(!isset($_SESSION['nombre'])){
			require_once "login.html";
		}
		else{
			header('location:index.php?action=perfil');
		}
	}
}


?>